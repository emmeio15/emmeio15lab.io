---
title: Ficha Técnica
layout: default
---

<div class="container-fluid">
  <div class="row">
<div class="ficha-tecnica col-sm-10">
  <h1>Ficha Técnica</h1>
  <p><b>Organização e curadoria:</b>Artur Cabral Reis, Suzete Venturelli e Taina Luize Martins Ramos
</p>
  
  <p><b>Assistentes de produção técnica e executiva:</b> Tainá Luize Martins Ramos, Lynn Carone, Nycacia Delmondes e Angélica Éfrem</p>
    <p><b>Apoio Institucional:</b> PPGAV/UnB (Sabrina Strasser e Ariel Machado)</p>


  <p><b><a href="mailto:medialabunbdf@gmail.com">Fale com a gente ;)</a></b></p>
  <p>
    <b>Artistas:</b><br>
    <span>
     Camille Venturelli ,Cora Venturelli, Revolution, Stable Diffusion e Suzete Venturelli<br>
Anna Amaral Rezende<br>
Rafa Cardozo, MediaLab / UNIFESSPA (Coordenação: Teófilo Augusto da Silva)<br>
Kelly Silva de Oliveira<br>
Bia Costa<br>
Cristiane Duarte, Rolf Simoes e Rosangela Leotte<br>
Darlith Cardoso Silva, Giovanny Lucas Silva Nascimento, Rogério Radiore Karaja, MediaLab / UNIFESSPA (Coordenação Teófilo Augusto da Silva)<br>
Tatiana dos Santos Duarte<br>
Jarda, MediaLab / UNIFESSPA (Coordenação: Teófilo Augusto da Silva)<br>
Lilian Amaral, Marcos Umpièrrez, Fabiane Santos, Liliana Fracasso, Marina Buj.<br>
Soraya Braz e Fabio FON<br>
Rosangella Leote<br>
Tainá Xavier<br>
Grupo de Pesquisa “Artes em Tecnologias Emergentes” FAAC Unesp Bauru<br>
Fabio FON <br>
Aquiles Burlamaqui e Laurita Salles<br>
Helena Hernández Acuaviva e Agda Carvalho<br>
Mateus Pelanda e Bianca Orsso<br>
Bruna Mazzotti e José Loures<br>
Alessandro Camilo e Sergio Nesteriuk<br>
Mércia de Assis Albuquerque<br>
Fernando Longhi, Ribs+Seixas<br>
Steven Eden Neto Nascimento MediaLab / UNIFESSPA<br>
Denise Camargo<br>
Pablo Gobira, Ítalo Travenzoli, Priscila Rezende Portugal<br>
Flávio Freire Carvalho
    </span>
  </p>
</div>
</div>
</div>


