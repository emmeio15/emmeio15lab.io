---
layout: obra
title: Delet&#233;rio
thumbnail: /assets/thumbnail/t-Deleterio.png
artista: Soraya Braz e Fabio FON
l-bio: fabiofon@gmail.com
t-bio: Fabio FON &#40;Fábio Oliveira Nunes&#41; &#233; artista&#45;pesquisador, Doutor em Artes &#40;ECA&#45;USP&#41; com pós&#45;doutorado em Artes &#40;IA&#45;UNESP&#41;. É autor dos livros “CTRL+ART+DEL&#58; distúrbios em arte e tecnologia” &#40;2010&#41; e “Mentira de artista&#58; arte &#40;e tecnologia&#41; que nos engana para repensarmos o mundo” &#40;2016&#41;. Atualmente &#233; vinculado ao Grupo cAt&#58; ci&#234;ncia&#47;ARTE&#47;tecnologia da UNESP, S&#227;o Paulo.
texto-descricao: Delet&#233;rio &#233; um videopoema produzido em computa&#231;&#227;o gráfica, criado por Soraya Braz e Fabio FON. Esta obra se inspira em um momento histórico recente do Brasil caracterizado pelo negacionismo cient&#237;fico e culto à viol&#234;ncia, combinados com uma roupagem “patriótica”, junto ao contexto da Pandemia de COVID&#45;19, que resultou em mais de 700 mil mortes. Como uma alegoria deste sinistro per&#237;odo, cria&#45;se um ambiente predominantemente verde&#45;amarelo sob o ritmo de acordes do hino nacional do Brasil em execu&#231;&#227;o por sons de tiros de revólver. Neste ambiente prevalece a palavra “delete”, que hoje se refere a apagar arquivos de computador, mas, em sua origem latina, já significou algo como “destrua isso” ou “erradique isso”. Os primeiros usos registrados na história de “dēlēte” se referiram a aniquilar ex&#233;rcitos inimigos e popula&#231;&#245;es. Assim, em sua constru&#231;&#227;o alegórica, quando “delete” &#233; continuamente reiterado, a obra busca evidenciar a obtusidade e a nocividade de discursos contemporâneos extremos. Vers&#227;o preliminar no YouTube &#40;sujeita a atualiza&#231;&#245;es&#41;&#58; https&#58;&#47;&#47;youtu.be&#47;sr9_EXNmCMU .
ano: 2023
tecnica: Video poema e computa&#231;&#227;o gráfica.

---

<iframe width="966" height="543" src="https://www.youtube.com/embed/sr9_EXNmCMU" title="Deletério" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>