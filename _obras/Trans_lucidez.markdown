---
layout: obra
title: Trans_lucidez
thumbnail: /assets/thumbnail/t-Trans_lucidez.png
artista: Bruna Mazzotti e Jos&#233; Loures
l-bio: #91;Jos&#233; Loures&#93; Artista multim&#237;dia, professor e produtor cultural. Doutor em Artes pela Universidade de Bras&#237;lia &#40;UnB&#41;. Mestre em Arte e Cultura Visual pela Universidade Federal de Goiás &#40;UFG&#41;. Pesquisador no Coletivo Interdisciplinar de Pesquisa em Games &#40;CIPEG&#41; e Comunidades Virtuais &#40;IFBaiano&#41;. Trabalha na linguagem da arte computacional, histórias em quadrinhos, webarte, fake arte e gamearte. Tamb&#233;m pesquisa sobre transhumanismo, videogames, jogos de tabuleiro, cibercultura, sexualidade e práticas divinatórias. &#91;Bruna Mazzotti&#93; Artista visual e arte educadora. Doutoranda em Arte e Cultura Visual &#40;UFG&#41;. Possui Mestrado em Artes Visuais &#40;UFRJ&#41;; &#233; especialista em Ensino de Artes Visuais &#40;Col&#233;gio Pedro II&#41;; e &#233; licenciada em Artes Visuais &#40;UFAM&#41;. Atua na área de Po&#233;ticas Visuais com &#234;nfase em&#58; performance arte, instala&#231;&#227;o, interven&#231;&#227;o, escrita aliada ao Tarô e proposi&#231;&#245;es colaborativas&#47;participativas para expandir lugares de enuncia&#231;&#227;o. Vinculada ao Comit&#234; de Po&#233;ticas Art&#237;sticas da Associa&#231;&#227;o Nacional de Pesquisadores em Artes Plásticas &#40;CPA&#47;ANPAP&#41;. Integrante do Núcleo de Práticas Art&#237;sticas Autobiográficas &#40;NuPAA&#47;FAV&#47;UFG&#47;CNPq&#41; na linha Materialidades e Imaterialidades Auto&#47;Biográficas nas po&#233;ticas art&#237;sticas e Processos de cria&#231;&#227;o.
texto-descricao: 74 pares de desenhos exibidos um v&#237;deo de 1 min. e 13 seg.
ano: 2020&#45;2023
tecnica: . Interven&#231;&#227;o&#58;
---
<iframe width="966" height="543" src="https://www.youtube.com/embed/FTCfs5oo4gE" title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
