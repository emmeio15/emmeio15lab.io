---
layout: obra
title: Memoriar
thumbnail: /assets/thumbnail/t-Memoriar.png
artista: Tainá Xavier
t-bio: Tainá Mara Moreira XavierMestranda em Artes Visuais, na linha de pesquisa Po&#233;ticas Transversais, do Programa de Pós&#45;gradua&#231;&#227;o em Artes Visuais do Instituto de Artes da Universidade de Bras&#237;lia &#40;PPGAV&#47;IDA&#47;UnB&#41;, licenciada e bacharel em Ci&#234;ncias Biológicas e bacharel em Museologia pela mesma institui&#231;&#227;o. Possui sua pesquisa po&#233;tica e produ&#231;&#227;o art&#237;stica direcionadas para linguagens da escrita e da imagem, como a fotografia, v&#237;deo e colagem e escrita.
texto-descricao: A obra consiste em uma alus&#227;o a como nossas memórias s&#227;o constru&#237;das. Seja em uma imagem fotográfica, na oralidade, ou ainda em um banco de dados e metadados, a montagem das memórias permite e engendra a imagina&#231;&#227;o. Compreender o inventar como parte essencial do processo de constru&#231;&#227;o de umas memórias contraria a necessidade da verdade final e absoluta sobre o que constitui nossas histórias e memórias. A obra &#233; composta por tr&#234;s express&#245;es e um mesmo acontecimento e exposto por meio de resultados de dispositivos distinto&#58; a fotografia, a oralidade e a cria&#231;&#227;o de imagens por meio de intelig&#234;ncia artificial generativa.
ano: 2023
tecnica: Videoarte
---
<iframe width="864" height="574" src="https://www.youtube.com/embed/nySWv0InpI4" title="MEMORIAR (sem crédito)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>