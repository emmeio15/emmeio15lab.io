---
layout: obra
title: Eîori! – um horizonte existencial
thumbnail: /assets/thumbnail/t-Ei.png
artista: Kelly Silva de Oliveira
l-bio: kellysol2011@gmail.com
t-bio: Kelly de Oliveira, mestranda em Design, pela Universidade Anhembi Morumbi&#47;SP e bacharel em Artes Visuais pela ECA&#45;USP &#40;2022&#41;. Por meio de pesquisa teórica e produ&#231;&#227;o audiovisual, busca aproximar&#45;se das cosmovis&#245;es dos povos originários e das suas po&#233;ticas sobre a exist&#234;ncia
texto-descricao:  No livro A queda do c&#233;u&#58; palavras de um xam&#227; yanomami, Davi Kopenawa afirma que o pensamento dos brancos &#233; confuso e obscuro, por conta dos ru&#237;dos das cidades e que “Eles dormem sem sonhos, como machados largados no ch&#227;o de uma casa” &#40;KOPENAWA e ALBERT, 2015, p. 76&#41;. Os xam&#227;s, ao contrário, s&#227;o levados para o ‘tempo do sonho’, onde aprendem ‘coisas de verdade’. É na perspectiva desse ‘tempo do sonho’ que se encontra Eîori! – um horizonte existencial, uma anima&#231;&#227;o que parte do relato de um dos sonhos que o xam&#227; yanomami, ainda crian&#231;a, costumava ter, quando os xapiri se aproximavam dele e carregavam a sua imagem para as alturas do c&#233;u. Partindo da&#237;, o relato se abre a um horizonte existencial para despertar a liberdade inventiva que rompe com a lógica que nos &#233; habitual. Eîori! &#40;em tupi antigo, Vem!&#41; carrega um paradoxo&#58; utiliza um recurso cuja narrativa &#233; intrinsecamente marcada pela sucess&#227;o de eventos em um tempo linear &#45; fruto do pensamento euroc&#234;ntrico &#45; para tratar da cosmovis&#227;o ind&#237;gena&#58; c&#237;clica, n&#227;o restrita ao tempo e ao espa&#231;o. Essa contradi&#231;&#227;o talvez seja uma forma de realizar o que o conjunto deste trabalho se prop&#245;e, isto &#233;, criar conex&#245;es, estabelecer alian&#231;as afetivas entre diferentes perspectivas de mundo.
ano: 2020&#45;22
tecnica: Anima&#231;&#227;o 2d e stopmotion.
---
<iframe width="966" height="543" src="https://www.youtube.com/embed/9h5EfV43hws" title="EÎORI! – UM HORIZONTE EXISTENCIAL  - Kelly Silva de Oliveira" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
