---
layout: obra
title: Lúcido e Cerrado
thumbnail: /assets/thumbnail/t-Lucido.png
artista: Bia Costa
l-bio: Bia Costa
 Mestranda em Design, Arte e Tecnologia n a Universidade Anhembi Morumbi e graduada na Escola de Comunica&#231;&#227;o e Artes da USP.
texto-descricao: Lúcido e Cerrado s&#227;o resultados de uma pesquisa sobre a rela&#231;&#227;o entre artista e máquina, de qual forma &#233; poss&#237;vel ceder o controle a um código e quais s&#227;o as possibilidades de varia&#231;&#227;o est&#233;tica na imagem final. A artista&#45;programadora criou um código utilizando de recursos como a sele&#231;&#227;o aleatória, deixando a posi&#231;&#227;o, tamanho, cor e forma dos itens com a maior varia&#231;&#227;o poss&#237;vel. As imagens apresentadas foram criadas pelo código e colocadas sem sele&#231;&#227;o ou altera&#231;&#227;o, consolidando como a artista cedeu o controle do resultado ao código.
ano: 2023
tecnica: Design arte generativa
---

<iframe width="372" height="662" src="https://www.youtube.com/embed/Ckyxu2ib4xY" title="LÚCIDO - Bia Costa" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<br><br>
<iframe width="372" height="662" src="https://www.youtube.com/embed/1apkfCjDOcY" title="Cerrado Bia Costa" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>