---
layout: obra
title: Cidade Perec&#237;vel 
thumbnail: /assets/thumbnail/t-Cidade.png
artista: Jarda, MediaLab &#47; UNIFESSPA,Coordena&#231;&#227;o&#58; Teófilo Augusto da Silva
l-bio: professor@teoaugusto.com.br
l-bio: Jarda, Artista visual, estudante do curso de Lic. em Artes Visuais na UNIFESSPA e Moradanovense &#40;morador do núcleo Morada Nova, localizado na cidade de Marabá&#45;PA&#41;. Sente proximidade com a fotografia e usa ela para expressar a est&#233;tica do cotidiano do núcleo em que vive, afim desvalorizar um dos bairros distantes do centro da cidade.<br>Teófilo Augusto da Silva, Artista, Professor e Pesquisador. Doutor em Artes Visuais &#40;Arte e Tecnologia&#41; pela UnB. Coordenador Media Lab&#47;Unifesspa.
texto-descricao: O “Cidade Perec&#237;vel” foi uma atividade criativa para uma disciplina prática do Curso de Artes Visuais da Universidade Federal do Sul e Sudeste do Pará que incentivava a cria&#231;&#227;o art&#237;stica entre os alunos. O projeto, come&#231;ou timidamente com algumas fotografias apreendidas de pequenos objetos e espa&#231;os da cidade de Marabá &#45; PA que na maioria das vezes poderiam ser ignorados pelos transeuntes. Depois, as imagens eram editadas em aplicativos no celular para alcan&#231;ar um aspecto mais dramático, chegando ao tom do ciano com alto contraste. Como aprofundamento será utilizado um mapa da cidade de Marabá com o recurso de realidade aumentada com a localiza&#231;&#227;o de onde as fotografias foram tiradas.
ano: 2023
tecnica: Fotografia digital com realidade aumentada.
---

<img src="/assets/obras/jarda/1.jpeg" alt="Cidade Perecível" class="img-fluid d-block">
<img src="/assets/obras/jarda/2.jpeg" alt="Cidade Perecível" class="img-fluid d-block">
<img src="/assets/obras/jarda/3.jpeg" alt="Cidade Perecível" class="img-fluid d-block">
<img src="/assets/obras/jarda/4.jpeg" alt="Cidade Perecível" class="img-fluid d-block">
<img src="/assets/obras/jarda/5.jpeg" alt="Cidade Perecível" class="img-fluid d-block">
<img src="/assets/obras/jarda/6.jpeg" alt="Cidade Perecível" class="img-fluid d-block">
<img src="/assets/obras/jarda/7.jpeg" alt="Cidade Perecível" class="img-fluid d-block">
<img src="/assets/obras/jarda/8.jpeg" alt="Cidade Perecível" class="img-fluid d-block">


