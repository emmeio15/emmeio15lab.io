---
layout: obra
title: A&#40;O&#41; banca&#40;o&#41;” sempre ganha!
thumbnail: /assets/thumbnail/t-abanca.png
artista: Alessandro Camilo e Sergio Nesteriuk
l-bio: alessandrocammilo@gmail.com
t-bio: Pesquisador dedicado a aprimorar o conhecimento do design com a intera&#231;&#227;o digital. Extenso conhecimento em Design, EAD e Tecnologia afins. Mestre e Doutorando em Design pela UAM, com 28 anos de experi&#234;ncia profissional.
texto-descricao:  A inten&#231;&#227;o desta obra &#233; apresentar de forma lúdica e at&#233; cômica o funcionamento do Atendimento ao Cliente, que muitas vezes pode ser confuso e at&#233; prejudicial na resposta às dúvidas dos clientes. O projeto utiliza como base a interface de comunica&#231;&#227;o e a experi&#234;ncia do usuário, mostrando como um bom design pode reorganizar a forma como as informa&#231;&#245;es s&#227;o transmitidas, resultando em caminhos mais adequados para a solu&#231;&#227;o de problemas em um suposto Design de Servi&#231;o, que &#233; um dos focos da tese em desenvolvimento. S&#227;o analisadas tr&#234;s fases&#58; atendimento humano, automatizado e robotizado, evidenciando os desafios e armadilhas de cada uma.
ano: 2023
tecnica: Webarte&#40;game art&#41;
---


<iframe class="frame" scrolling="no" src="http://projetosacademicos.coisasemdesign.com.br/proj01/"></iframe>
<br>
<a href="https://projetosacademicos.coisasemdesign.com.br/proj01/" target="_blank">TELA INTEIRA</a>
