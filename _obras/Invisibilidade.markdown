---
layout: obra
title: Invisibilidade artr&#237;tica
thumbnail: /assets/thumbnail/t-ONDAS.png
artista: Rosangella Leote
l-bio: rosangellaleote@gmail.com
t-bio: Rosangella Leote &#40;SP&#47;BR&#41; &#233; artista&#47;pesquisadora em Arte&#47;Ci&#234;ncia&#47;Tecnologia; Bolsa de Produtividade em Pesquisa &#40;CNPq_B2&#41; pesquisando arte e sustentabilidade com impress&#227;o 3D; Pós&#45;doutora &#40;UAb&#45;PT – FAPESP&#41;; Doutora em Ci&#234;ncias da Comunica&#231;&#227;o &#40;USP&#47;BR – CNPq&#41;.Trabalha em Artemedia envolvendo realidade virtual, instala&#231;&#245;es, esculturas e objetos interativos, “tecnoperformances” e outros. É l&#237;der do grupo de pesquisa GIIP &#40;IA&#47;Unesp&#41;.
texto-descricao: Invisibilidade artr&#237;tica &#233; um vest&#237;vel com proje&#231;&#227;o de imagens de ressonância magn&#233;tica dacoluna e f&#234;mur da artista, onde se pronunciam altera&#231;&#245;es artr&#237;tico&#45;degenerativas osteodiscal einterapofisárias, com h&#233;rnia de disco. A visualiza&#231;&#227;o das diferentes camadas das ressonâncias e eventos sonoros se formam pela intera&#231;&#227;o com o público atrav&#233;s de ultrassons mediados por machine&#45;learning. A obra torna vis&#237;vel degenera&#231;&#245;es que só podem ser supostas atrav&#233;s da dor e conhecidas atrav&#233;s da visualiza&#231;&#227;o pela máquina de ressonância. A intera&#231;&#227;o com o público aciona a ideia de que, tamb&#233;m atrav&#233;s do som, o invis&#237;vel destes corpos que interagem, se atualiza aos olhos dos outros e aos seus mesmos. É tamb&#233;m uma imers&#227;o na inescapabilidade aos efeitos do tempo, no corpo e na mente.
ano: 2023
tecnica: Performance
---

