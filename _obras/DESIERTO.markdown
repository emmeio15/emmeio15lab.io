---
layout: obra
title: DESIERTO&#47;INUNDACIÓN – PAISAGENS EXTREMAS. Obra co&#45;elaborativa, processual, aberta e em rede.
thumbnail: /assets/thumbnail/t-DESIERTO.png
artista: Lilian Amaral &#40;Lilian do Amaral Nunes – Media Lab UFG&#47;Brasil &#45; autor principal&#41;, Marcos Umpièrrez &#40;IENBA&#47;UDELAR – Uruguay&#41;, FaBiane Santos &#40;Bia Santos – Universidade de Zaragoza&#45; Espanha&#41;, Liliana Fracasso &#40;Academia de Belas Artes de Veneza&#45;Itália&#41;, Marina Buj &#40;Universidade de Gorina&#45;Espanha&#41;
l-bio: lilianamaraln@gmail.com
t-bio: O grupo de Pesquisa Internacional e interinstitucional de co&#45;pesquisa e co&#45;cria&#231;&#227;o em arte, ci&#234;ncia e tecnologia em rede HolosCi&#91;u&#93;da&#91;e&#93; busca propor e analisar experi&#234;ncias multimetodológicas para compreens&#227;o da complexidade da cidade contemporânea, no atual contexto de espetaculariza&#231;&#227;o urbana, buscando articular linhas de abordagem que costumam ser tratadas separadamente&#58; das práticas art&#237;sticas em perspectiva relacional, apreens&#227;o critica à experi&#234;ncia est&#233;tica&#45;corporal urbana. Configura&#45;se como prática co&#45;elaborativa, a partir de elementos audiovisuais &#47; multissensoriais que estabelecem conex&#227;o entre os diversos núcleos de pesquisa interligados por meio das Redes, Observatórios e MediaLabs distribu&#237;dos e atuantes em contexto ibero americano &#45; http&#58;&#47;&#47;www.espai214.org&#47;holos&#47;
texto-descricao: DESIERTO&#47;INUNDACIÓN – Trabalho colaborativo composto por 20 narrativas sonoro&#45;visuais e textuais, oriundas de diferentes lugares geográficos do mundo especialmente sens&#237;veis, que comp&#245;em um ambiente de experimenta&#231;&#227;o e explora&#231;&#227;o multissensorial. As bolhas que emergem em contexto virtual e interativo s&#227;o o dispositivo experiencial que no metaverso convida o interator&#47;usuário a participar de percep&#231;&#245;es situadas da obra DESERTO&#47;INUNDACIÓN. A multiplicidade e transforma&#231;&#227;o das paisagens afetadas pela acelerada mudan&#231;a climática configuram XENOPAISAGENS. A água continua a ser o fio condutor da co&#45;cria&#231;&#227;o e co&#45;investiga&#231;&#227;o proposta pelo Grupo de Pesquisa HOLOSCI&#91;U&#93;DAD&#91;E&#93;.
ano: 2023
tecnica: 
---

<iframe class="frame" scrolling="no" src="https://www.spatial.io/s/Desierto-inundacion-6475e4177ba0e76e3a1736df?share=8140543362655784075"></iframe>
<br>
<a href="https://www.spatial.io/s/Desierto-inundacion-6475e4177ba0e76e3a1736df?share=8140543362655784075" target="_blank">TELA INTEIRA</a>