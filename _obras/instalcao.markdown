---
layout: obra
title: Instala&#231;&#227;o Interativa em realidade virtual &#34;Museu em chamas&#34; &#40;2023&#41;
thumbnail: /assets/thumbnail/t-instalcao.png
artista: Pablo Gobira, Ítalo Travenzoli, Priscila Rezende Portugal
l-bio: Priscila Rezende Portugal &#233; mestranda em Gest&#227;o e Organiza&#231;&#227;o do Conhecimento na Escola de Ci&#234;ncia da Informa&#231;&#227;o da UFMG. Bacharela em Artes Plásticas pela Escola Guignard &#40;UEMG&#41;. É membro do grupo de pesquisa, inova&#231;&#227;o e desenvolvimento CNPq Laboratório de Po&#233;ticas Fronteiri&#231;as &#40;LabFront&#41; desde 2018, como bolsista de inicia&#231;&#227;o cient&#237;fica PAPq&#47;PIBIC &#40;2018, 2019&#41; e inicia&#231;&#227;o tecnológica PIBITI &#40;2020&#41;, no qual pesquisa os atravessamentos entre arte, ci&#234;ncia e tecnologia.<br>Pablo Alexandre Gobira de Souza Ricardo &#91;PT&#45;BR&#93; &#233; Professor da Escola Guignard &#40;UEMG&#41;, do PPGArtes &#40;UEMG&#41;, do PPGACPS &#40;UFMG&#41; e do PPGGOC &#40;UFMG&#41;. Coordenador e membro de Câmara de Assessoramento de Ci&#234;ncias Humanas, Sociais, Educa&#231;&#227;o, Letras e Artes da FAPEMIG. Avaliador AD HOC da European Science Foundation, CNPq, CAPES, FAPESB dentre outras ag&#234;ncias nacionais e internacionais e pesquisador produtividade &#40;CNPq&#41;. Coordenador do grupo de pesquisa &#40;CNPq&#41; Laboratório de Po&#233;ticas Fronteiri&#231;as &#91;http&#58;&#47;&#47;labfront.weebly.com. Atua no campo da cultura como curador, escritor e editor de diversos livros .
texto-descricao: #34;Museu em chamas&#34; &#233; uma instala&#231;&#227;o interativa em realidade virtual constru&#237;da como uma game arte. Ao interator &#233; permitido experimentar a situa&#231;&#227;o do museu sofrendo um inc&#234;ndio. O ambiente constru&#237;do &#233; inspirado no Museu Nacional e o inc&#234;ndio que o destruiu em 2 de setembro de 2018 e acaba lan&#231;ando luz sobre sobre a perda do patrimônio art&#237;stico e cultural. A a&#231;&#227;o po&#233;tica tamb&#233;m se ancora nas discuss&#245;es conceituais que operaram há mais de 100 anos no contexto vanguardista, bem como dialoga com ideia de &#34;contemporâneo&#34; em Giorgio Agamben, discutida em artigo publicado por membros do LabFront em 2022 &#40;&#34;O museu em chamas&#58; a perda do patrimônio e as tecnologias digitais sob a luz de um inc&#234;ndio&#34;, de Pablo Gobira e Priscila Rezende Portugal, 2022&#41;.
ano: 2023
tecnica: Game arte e Realidade Virtual
---


<iframe class="frame" scrolling="no" src="https://labfront.weebly.com/omuseuemchamas.html"></iframe>
<br>
<a href="https://labfront.weebly.com/omuseuemchamas.html" target="_blank">TELA INTEIRA</a>