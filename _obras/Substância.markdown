---
layout: obra
title: Substância Hidrofóbica Abstrata
thumbnail: /assets/thumbnail/t-Substancia.png
artista: Mateus Pelanda e Bianca Orsso
l-bio: mateuspelanda@gmail.com
t-bio: Mateus F. Lima Pelanda &#233; designer, artista e programador. Mestre em Tecnologia e Sociedade &#40;PPGTE – UTFPR&#41; e bacharel em Design pela mesma institui&#231;&#227;o &#40;UTFPR&#41;. Em sua pesquisa busca desenvolver e explorar diferentes suportes em arte digital e eletrônica.Bianca Orsso &#233; artista multim&#237;dia, mestranda em Tecnologia e Sociedade &#40;PPGTE&#47;UTFPR&#41; e formada em Artes Visuais &#40;FAP&#47;UNESPAR&#41;. Em sua pesquisa po&#233;tica busca explorar as intersec&#231;&#245;es entre as artes eletrodigitais e suportes analógicos, como a pintura e a escultura.
texto-descricao: Neste trabalho buscamos emular, de modo abstrato, a intera&#231;&#227;o de substâncias hidrofóbicas. Nesse caso, o ponteiro do mouse representa uma mol&#233;cula insolúvel, n&#227;o polarizada, como o óleo, ao mesmo passo em que as formas retangulares representam as part&#237;culas de outro solvente, como a água. Para a cria&#231;&#227;o deste trabalho, utilizamos como base exemplos de arco tangente encontrados na plataforma Openprocessing e na IDE Processing.
ano: 2023
tecnica: Software arte.
---

<iframe class="frame" scrolling="no" src="https://openprocessing.org/sketch/1606147/embed/"></iframe>
<br>
<a href="https://openprocessing.org/sketch/1606147/embed/" target="_blank">TELA INTEIRA</a>
