---
layout: obra
title: Mitos Vadios
thumbnail: /assets/thumbnail/t-Mitos.png
artista: Grupo de Pesquisa “Artes em Tecnologias Emergentes” – da FAAC Unesp Bauru &#40;Faculdade de Arquitetura, Artes, Comunica&#231;&#227;o e Design&#41;.
l-bio: sidney.tamai@unesp.br
t-bio: Sidney Tamai&#58; Arquiteto, doutor pela Fau&#45;Usp na área de Design e Arquitetura, mestre em Artes e Multimeios pela Unicamp. Professor pesquisador do curso de Artes da Faac Unesp Bauru. Tem estudado e experimentado o campo transdisciplinar entre artes, design e arquitetura com foco nas possibilidades dos novos dispositivos t&#233;cnicos, suas linguagens, procurando dar destino po&#233;tico ao improvável. Grupo de Pesquisa ARTES EM TECNOLOGIAS EMERGENTES&#58; Faac Unesp Bauru. dgp.cnpq.br&#47;dgp&#47;espelhogrupo&#47;4720563882734500 &#45; Linha de Pesquisa&#58; Arte, Inova&#231;&#227;o e Tecnologia. A pesquisa do grupo &#233; sobre como produzir po&#233;ticas art&#237;sticas articulando interfaces dos novos dispositivos tecnológicos de software&#47;hardware abertos e contemporâneos. Virtual e atual s&#227;o entendidos como prática em novos horizontes para descoberta de improváveis. Integrantes&#58; coordenador&#47;l&#237;der Sidney Tamai; vice&#45;l&#237;der Luiz Antônio Vasques Hellmeister; Integrantes pesquisadores&#58; Dorival Campos Rossi &#40;professor Design Faac Unesp&#41;, Thiago Stefanin &#40;artista e professor&#41;, Ailton Wenceslau Silva Junior &#40;arquiteto, músico e artista&#41;; Estudantes, artistas e pesquisadores no curso de Artes Faac Unesp&#45;Bauru&#58; Fernanda Catelani Branquinho e Dennis Marcio de Oliveira Gomes.
texto-descricao: Trata&#45;se de um v&#237;deo in&#233;dito de 8 minutos da apresenta&#231;&#227;o de vários artistas, o que inclui H&#233;lio Oiticica, no evento dos Mitos Vadios em estacionamento da rua Augusta em 1979
ano: 2023
tecnica: V&#237;deo
---

<iframe width="966" height="543" src="https://www.youtube.com/embed/fClC_ihz51c" title="MITOS VADIOS – super 8,56” – vídeo inédito 1978 (FULLAUDIO)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

