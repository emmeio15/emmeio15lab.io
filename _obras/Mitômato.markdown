---
layout: obra
title: Mitômato
thumbnail: /assets/thumbnail/t-Mitomato.png
artista: Fabio FON &#40;Fabio Oliveira Nunes&#41;
l-bio: fabiofon@gmail.com
t-bio: Fabio FON &#40;Fábio Oliveira Nunes&#41; &#233; artista&#45;pesquisador, Doutor em Artes &#40;ECA&#45;USP&#41; com pós&#45;doutorado em Artes &#40;IA&#45;UNESP&#41;. É autor dos livros “CTRL+ART+DEL&#58; distúrbios em arte e tecnologia” &#40;2010&#41; e “Mentira de artista&#58; arte &#40;e tecnologia&#41; que nos engana para repensarmos o mundo” &#40;2016&#41;. Atualmente &#233; vinculado ao Grupo cAt&#58; ci&#234;ncia&#47;ARTE&#47;tecnologia da UNESP, S&#227;o Paulo.
texto-descricao: ChatGPT &#233; um popular robô de conversa&#231;&#227;o com intelig&#234;ncia artificial, desenvolvido pela empresa OpenAI, que responde por texto a quest&#245;es de seus interlocutores. Mitômano &#233; algu&#233;m que mente compulsivamente, de forma patológica. Por sua vez, Mitômato &#40;mitômano + autômato&#41; apresenta o resultado de um experimento simples com o ChatGPT&#58; perguntar repetidamente sobre vida e obra do artista brasileiro Fabio FON, autor deste experimento. O robô sempre tenta cumprir o que foi pedido seguindo os padr&#245;es de biografias de artistas, só que, na maioria das vezes, acaba misturando algumas informa&#231;&#245;es pouco precisas com muita informa&#231;&#227;o falsa, inclusive indicando obras de outros criadores como se tivessem sido criadas por FON. Nesta exibi&#231;&#227;o, o v&#237;deo apresenta a transcri&#231;&#227;o de trechos das conversas com o ChatGPT, realizadas entre os meses de fevereiro e mar&#231;o de 2023, destacando em tachado os trechos com conteúdo absolutamente mentiroso. Dispon&#237;vel no YouTube&#58; https&#58;&#47;&#47;youtu.be&#47;ume_20S_6V8 .
ano: 2023
tecnica: V&#237;deo com intelig&#234;ncia artificial
---

<iframe width="966" height="543" src="https://www.youtube.com/embed/ume_20S_6V8" title="Mitômato (2023)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>