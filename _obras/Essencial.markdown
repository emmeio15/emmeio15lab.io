---
layout: obra
title: Essencial
thumbnail: /assets/thumbnail/t-Essencial.png
artista: Rafa Cardozo, MediaLab &#47; UNIFESSPA, Coordena&#231;&#227;o&#58; Teófilo Augusto da Silva
l-bio: professor@teoaugusto.com.br
t-bio: Rafa Cardozo, Artista visual, graduanda em Lic. em Artes Visuais &#40;UNIFESSPA&#41; e integrante do grupo de pesquisa Media LAB&#47;UNIFESSPAA. Nascida e criada no Sudeste do Pará, onde encontra inspira&#231;&#227;o no calor e na viv&#234;ncia na cidade de Marabá. Tem interesse principal na arte&#45;educa&#231;&#227;o e fotografia, mas experimenta outras linguagens art&#237;sticas. Nessas produ&#231;&#245;es, faz reflex&#227;o sobre identidade, pensando em como &#233; formada nas rela&#231;&#245;es, no meio em que vive e no próprio interior.<br>Teófilo Augusto da Silva, Artista, Professor e Pesquisador. Doutor em Artes Visuais &#40;Arte e Tecnologia&#41; pela UnB. Coordenador Media Lab&#47;Unifesspa.
texto-descricao: Descri&#231;&#227;o&#58; a obra “essencial” &#233; um auto retrato em formato de videoperformance, que traz como tema de reflex&#227;o o próprio interior, as quest&#245;es de identidade e a percep&#231;&#227;o de si mesmo. Onde a artista utiliza de texto, corpo, voz e cores que tentem remeter sensibilidade e vulnerabilidade.
ano: 2023
tecnica: Videoperformance
---

<iframe width="966" height="543" src="https://www.youtube.com/embed/L1orxiXVSwk" title="ESSENCIAL - Rafa Cardozo, MediaLab / UNIFESSPA, Coordenação: Teófilo Augusto da Silva" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<iframe width="966" height="543" src="https://www.youtube.com/embed/gurNnBGeIvY" title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>