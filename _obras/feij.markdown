---
layout: obra
title: Feij&#227;o Aben&#231;oado &#40;sacred bean&#41;
thumbnail: /assets/thumbnail/t-feij.png
artista: Steven Eden Neto Nascimento MediaLab &#47; UNIFESSPA
l-bio: professor@teoaugusto.com.br
l-bio: Steven Eden Neto Nascimento, Graduando em Artes Visuais pela Universidade Federal do Sul e Sudeste do Pará &#40;UNIFESSPA&#41;, campus Marabá. Atualmente participante do projeto Media Lab&#47;UNIFESSPA. Ilustrador freelancer.<br>Media Lab&#47;Unifesspa&#58; laboratório interdisciplinar e núcleo de pesquisa e extens&#227;o da Universidade Federal do Sul e Sudeste do Pará &#45; Unifesspa. Fundado em 2016, ingressou imediatamente na Rede Media Lab&#47;BR, atuando assim para a pesquisa teórico&#45;prática envolvendo a rela&#231;&#227;o entre Arte, Ci&#234;ncia e Tecnologia.<br>Teófilo Augusto da Silva, Artista, Professor e Pesquisador. Doutor em Artes Visuais &#40;Arte e Tecnologia&#41; pela UnB. Coordenador Media Lab&#47;Unifesspa.
texto-descricao: A obra apresenta uma intera&#231;&#227;o ligada à realidade aumentada atrav&#233;s da ferramenta de convers&#227;o dispon&#237;vel no site Artivive onde ao ser reconhecida por um aplicativo &#40;dispon&#237;vel para Android e IOS&#41; de mesmo nome do site revela uma anima&#231;&#227;o que complementa a obra em quest&#227;o.
ano: 2023
tecnica: Imagem digital e obra em Realidade Aumentada
---

<a href="https://play.google.com/store/apps/details?id=com.artivive&hl=pt" target="_blank">Link para o APP</a>
