---
layout: obra
title: Cartas Vozeris
thumbnail: /assets/thumbnail/t-Cartas.png
artista: Tatiana dos Santos Duarte
t-bio: Tatiana Duarte &#233; performer e busca for&#231;as nos feminismos nômades, desenterrando algo que n&#227;o foi dito&#58; de um apagamento. Uma aprendiz que redescobre caminhos e fissuras. Doutoranda em Artes Visuais pela UnB, mestra em Artes Visuais pela UFPel, licenciada em Teatro pela UFPel. Pesquisa os processos de cria&#231;&#227;o e po&#233;ticas do cotidiano e coloca o corpo como suporte em performance. Desenterra algo que n&#227;o foi falado&#58; a voz remove os silenciamentos.
texto-descricao: Um jogo de cartas performático que prop&#245;e a retirada das palavras pejorativas comumente atribu&#237;das aos g&#234;neros. Ao se deparar com as cartas, o consulente escolherá uma imagem e um poema a serem escutados. Cada carta conta com uma animalidade, como vaca, cadela e piranha. O jogo engendra um confronto contundente contra os preconceitos que adjetivam o g&#234;nero pela animalidade, que &#233; utilizada como instrumento de opress&#227;o. As cartas d&#227;o outros contornos de pot&#234;ncia po&#233;tica ao apropriar&#45;se destes temos. No decorrer do jogo, as cartas se materializam à vista do público, que, de forma aleatória, &#233; convidado a acionar a sua escolha entre as treze cartas dispon&#237;veis. Ao selecionar uma destas cartas, lan&#231;a&#45;se a leitura dos poemas, as vozes de lutas contra os machismos estruturais.<br><br>Ano&#58; 2023.<br>Poemas e imagens&#58; Tatiana Duarte<br>Ficha t&#233;cnica&#58;<br>Programador&#58;<br>Andrey Rosa Neves<br>Vozes&#58;<br>1&#45;Piranha&#45; Núbia Thalita e Carla Maria Alves Rocha<br>2&#45;Lesma&#45; Laura Nunes<br>3&#45;Barata&#45;Lumilan Noda Vieira<br>4&#45;Anta&#45; Bia Medeiros<br>5&#45; Aranha&#45; Rejanete Vieira<br>7&#45;Egua&#45; Stefane Leal<br>8&#45;Formiga&#45;Ivana Motta<br>9&#45; Galinha&#45; Juliana Cerqueira e Thiane Nascimento Ferreira<br>10&#45;Jararaca&#45;Bia Medeiros<br>11&#45;Topeira&#45; Jailton Pontes<br>12&#45; Vaca&#45; Alexandra Dias<br>13&#45;Arlequina&#45;Daniela Duarte Awarana
ano: 2023
tecnica: Game&#45;arte
---
<iframe class="frame" scrolling="no" src="/assets/obras/jogo-carta/"></iframe>
<br>
<a href="/assets/obras/jogo-carta/" target="_blank">TELA INTEIRA</a>
