---
layout: obra
title: Incita&#231;&#245;es #01
thumbnail: /assets/thumbnail/t-insita.png
artista: Denise Camargo
l-bio: denise.cfcamargo@gmail.com
t-bio: Denise Camargo &#233; artista visual, atua em ensino, pesquisa, curadoria e gest&#227;o de projetos art&#237;sticos e culturais. A escrita e as imagens fotográficas s&#227;o mat&#233;ria para sua produ&#231;&#227;o. É Doutora em Artes &#40;IA&#47;Unicamp&#41; e Mestra em Ci&#234;ncias da Comunica&#231;&#227;o &#40;ECA&#47;USP&#41; e docente na gradua&#231;&#227;o do Departamento de Artes Visuais&#47;Instituto de Artes&#47;Universidade de Bras&#237;lia e do Programa de Pós&#45;gradua&#231;&#227;o em Artes Visuais.
texto-descricao: S&#233;rie de imagens geradas por intelig&#234;ncia artificial para integrar a experi&#234;ncia coletiva e colaborativa da publica&#231;&#227;o “A mulher que virou beija&#45;flor” &#40;2023&#41;, um projeto coordenado pelo curador e editor Eder Chiodetto. O livro está no preto, em edi&#231;&#227;o pela Fotô Editorial, especializada na produ&#231;&#227;o de livros de fotografia autoral e de reflex&#227;o acerca do estatuto da imagem contemporânea. As imagens geradas na plataforma Midjourney criam uma narrativa para a passagem do livro em que uma borboleta se transforma em um beija&#45;flor. As imagens s&#227;o apresentadas em v&#237;deo, por meio dos prompts que as geraram. O trabalho procura abordar o impacto do texto na gera&#231;&#227;o dessa natureza de imagem, em seu processo de fabula&#231;&#227;o.
ano: 2023
tecnica: V&#237;deo
---
<iframe width="966" height="543" src="https://www.youtube.com/embed/XNtlXFcXtss" title="Incitações- Denise Camargo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
