---
layout: obra
title: V&#237;deo&#45;Atlas de Futuros Urbanos&#58; a Surdez do Imaginário
thumbnail: /assets/thumbnail/t-atlas.png
artista: Fernando Longhi, Ribs+Seixas
l-bio: longhi@usp.br
t-bio: Fernando Longhi &#233; Arquiteto e Urbanista pela FAU&#45;UnB e atualmente &#233; mestrando e pesquisador da Universidade de S&#227;o Paulo &#40;FAUUSP&#47;IEA&#45;USP&#41;, onde estuda a converg&#234;ncia entre tecnologias de intelig&#234;ncia e cultura digital em escala urbana. RIBS+SEIXAS &#233; um duo criativo formado por Pedro Ribs e Lucas Seixas dedicado ao design e artes visuais, atuando na dire&#231;&#227;o de arte, motion design e anima&#231;&#227;o. Com exerc&#237;cio multidisciplinar, realizam campanhas, documentários e instala&#231;&#245;es de arte para clientes como MIT Media Lab, Netflix e Multishow.
texto-descricao: #34;Video&#45;Atlas de Futuros Urbanos&#58; a Surdez do Imaginário&#34; mergulha em um mundo hibridizado, surreal e abstrato, onde testemunhamos a constru&#231;&#227;o de um repertório de imagens de futuros urbanos engendrado pelos tentáculos invis&#237;veis da imagina&#231;&#227;o atrelada a um dom&#237;nio algor&#237;tmico. O filme&#45;ensaio &#233; uma manifesta&#231;&#227;o visual que busca explorar o imaginário de futuros urbanos em uma XENOpaisagem que mira al&#233;m da linguagem textual e das telas estáticas. Ao se estabelecer como uma express&#227;o art&#237;stica que usufrui da metalinguagem das imagens em fluxo, o V&#237;deo&#45;Atlas convida a refletir sobre a surdez das imagens em um mar de r&#233;plicas digitais e a buscar um despertar coletivo para ouvir al&#233;m do pano umbroso do virtual, desvendando as camadas ocultas de um mundo visualmente saturado. O “pano umbroso do virtual” encaixa&#45;se em uma das XENOpaisagens do contemporâneo, integrando as espacialidades hibridizadas proporcionadas pelas tecnologias digitais.
ano: 2023
tecnica: V&#237;deo e arte digital.
---

<iframe width="966" height="543" src="https://www.youtube.com/embed/IZiJLBhKgfY" title="VÍDEO-ATLAS DE FUTUROS URBANOS: A SURDEZ DO IMAGINÁRIO - Fernando Longhi, Ribs Seixas." frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>