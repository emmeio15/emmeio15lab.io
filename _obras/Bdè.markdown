---
layout: obra
title: Bdè tmyrà &#45; Renova&#231;&#227;o
thumbnail: /assets/thumbnail/t-bde.png
artista: Darlith Cardoso Silva, Giovanny Lucas Silva Nascimento, Rog&#233;rio Radiore Karaja, MediaLab &#47; UNIFESSPA, Coordena&#231;&#227;o Teófilo Augusto da Silva
l-bio: professor@teoaugusto.com.br
t-bio: Media Lab&#47;Unifesspa&#58; laboratório interdisciplinar e núcleo de pesquisa e extens&#227;o da Universidade Federal do Sul e Sudeste do Pará &#45; Unifesspa. Fundado em 2016, ingressou imediatamente na Rede Media Lab&#47;BR, atuando assim para a pesquisa teórico&#45;prática envolvendo a rela&#231;&#227;o entre Arte, Ci&#234;ncia e Tecnologia. Teófilo Augusto da Silva, Artista, Professor e Pesquisador. Doutor em Artes Visuais &#40;Arte e Tecnologia&#41; pela UnB. Coordenador Media Lab&#47;Unifesspa.
texto-descricao: #34;Bdè tmyrà &#45; Renova&#231;&#227;o&#34; &#233; uma instala&#231;&#227;o art&#237;stica que incorpora tr&#234;s fotografias em preto e branco, cada uma representando s&#237;mbolos centrais do criacionismo Karajá, uma etnia ind&#237;gena brasileira. A primeira fotografia retrata um homem Karajá com pintura corporal e temporárias, personificando Kananciu&#234;, o criador, cuja representa&#231;&#227;o abstrata do pesco&#231;o para baixo evoca a aura divina. Ele mantinha suas m&#227;os abertas, deixando escorrer terra e água, simbolizando o ato primordial da cria&#231;&#227;o. A segunda fotografia apresenta uma imponente árvore que personifica o conceito Karajá de &#34;kuni&#34;, representando as almas e a for&#231;a da natureza em sua cosmovis&#227;o. Com um tronco forte e uma copa exuberante, a árvore simboliza a resist&#234;ncia e o poder das almas que ela protege. A terceira fotografia registra as escamas de peixe, representando os seres que, segundo a cren&#231;a Karajá, originaram os seres humanos, os peixes&#45;aruan&#227;. Essa imagem está associada à origem primordial e ancestral da humanidade, conectando os seres humanos à natureza e sua interdepend&#234;ncia com os outros seres vivos. A obra busca tecer uma cr&#237;tica à imposi&#231;&#227;o unilateral da religi&#227;o e cultura europeia como única e verdadeira, enquanto procura evidenciar o colonialismo estrutural e sua tend&#234;ncia histórica em silenciar e apagar outras culturas, especialmente as das etnias ind&#237;genas. A arte pretende destacar a importância de reconhecer as perspectivas culturais diversas.
ano: 2023
tecnica: Fotografia, pintura e realidade aumentada
---

<img src="/assets/obras/Bde/3.JPG" alt="" class="img-fluid d-block">
<img src="/assets/obras/Bde/2.JPG" alt="" class="img-fluid d-block">
<img src="/assets/obras/Bde/1.JPG" alt="" class="img-fluid d-block">

