---
title: Sobre
layout: default
---

<div class="container-fluid">
  <div class="row">
    <div class="sobre col-sm-10">
      <h5>
        <br><br>
Nesta edição, o tema central é "XENOpaisagens", cujo termo é um neologismo criado para descrever a transformação das paisagens naturais devido a fenômenos climáticos e tecnológicos, os eventos discutirão a relação entre a arte, ciência e tecnologia para despertar emoções, sensibilizar e conscientizar as pessoas sobre os impactos do conhecimento universitário, sociais e políticos da atualidade. Através da arte, é possível transmitir mensagens poderosas e estimular reflexões profundas.
      </h5>
      <p class='nome-autor'></p>
    </div>
  </div>
</div>
